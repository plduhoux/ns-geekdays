import Vue from 'nativescript-vue';
import {TNSFontIcon, fonticon} from 'nativescript-fonticon';

import GeekDays from '@/components/GeekDays';
import RadListView from 'nativescript-ui-listview/vue'
import RadSideDrawer from 'nativescript-ui-sidedrawer/vue'
import {Ripple} from "nativescript-ripple"

// Uncommment the following to see NativeScript-Vue output logs
Vue.config.silent = false;

//Initialize FontAwesome
TNSFontIcon.debug = true;
TNSFontIcon.paths = {
    'fa': './fonts/fa5-all.css'
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);

Vue.use(RadListView)
//Ripple element to add ripple to label buttons
Vue.registerElement("Ripple", () => Ripple);

new Vue({

    render: h => h('frame', [h(GeekDays)])
    

}).$start();
