import * as appSettings from "tns-core-modules/application-settings";
import specialNumbers from "./specialNumbers"
import moment from "moment"
import { ObservableArray } from 'tns-core-modules/data/observable-array';

ObservableArray.prototype.find = function(fn) {
    for (let i = 0; i < this.length; i++) {
        if (fn(this.getItem(i))) return this.getItem(i)
    }
    return undefined
}

class DaysService {
    constructor() {
        this.firsttime = true
        let str = appSettings.getString("birthday")
        if (str) {
            this.birthday = moment(appSettings.getString("birthday"))
            this.firsttime = false
        } else {
            this.birthday = moment("October 30, 1985")
        }

        this.nextDaysList = new ObservableArray([])
        this.nextDays()

        this.options = []
        let opts = appSettings.getString("options")
        if (opts) {
            this.options.push(...JSON.parse(opts))
        } else {
            this.options.push(...Object.keys(specialNumbers).map(key => {
                return {
                    name: key,
                    checked: true
                }
            }))
            console.log(this.options)
        }
    }

    setBirthday(date) { // here is a real Date, no moment
        this.birthday = moment(date)
        appSettings.setString("birthday", this.birthday.format())
        this.nextDays()
    }

    saveOptions() {
        //appSettings.setString("options", JSON.stringify(this.options))
    }
    setOption(name, checked) {
        let opt = this.options.find(opt => opt.name === name)
        if (opt !== undefined) {
            opt.checked = checked
        } else {
            this.options.push({
                name: name,
                checked: checked
            })
        }
        this.saveOptions()
    }
    getOption(name) {
        let opt = this.options.find(opt => opt.name === name)
        if (opt !== undefined) return opt.checked
        return true
    }
    nextDays() {
        let lst = []
        for (let genName in specialNumbers) {
            lst.push(...this.genNextDays(genName, 2))
        }
        lst.sort((a, b) => a.date.diff(b.date))

        this.nextDaysList.length = 0;
        this.nextDaysList.push(...lst)
    }

    genNextDays(genName, nb) {
        let gen = specialNumbers[genName]
        let lst = []
        for (let dtype of gen.types) {
            let start = moment().diff(this.birthday, dtype)
            //for years and month, add 1 or we could have past dates (except if it's birthday / monthday)
            if (dtype === "years") {
                if (moment().month() !== this.birthday.month() ||
                    moment().date() !== this.birthday.date()) {
                    start += 1
                }
            } else if (dtype === "months") {
                if (moment().date() !== this.birthday.date()) {
                    start += 1
                }
            }
            let generator = gen.generator(start)
            for (let i = 0; i < nb; i++) {
                //get next value
                let genVal = generator.next()
                //test if generator is done
                if (genVal.value === undefined && genVal.done === true) break;

                // generate entry
                let next = genVal.value
                let nextValue = next
                if (typeof gen.getValue === 'function') nextValue = gen.getValue(next)
                lst.push({
                    date: this.birthday.clone().add(nextValue, dtype),
                    value: nextValue,
                    res: next,
                    type: dtype,
                    gen: genName
                })
            }
        }
        // Filter no more than 120 years of life
        lst = lst.filter(d => d.date.diff(this.birthday, "years") <= 120)
                 .sort((a, b) => a.date.diff(b.date))
        return lst.slice(0, nb)
    }
}

export default new DaysService()