import regeneratorRuntime from "regenerator-runtime";
import moment from "moment"

/**
 * Generators for special numbers
 */
export default {
    primes: {
        name: "Prime numbers",
        types: ["years", "months", "days"],
        explain: val => "Prime number",
        generator: function* (start) {
            let i = start
            while (true) {
                if (isPrime(i)) yield i
                i++
            }
        }
    },
    twopows: {
        name: "Powers of two",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => {
            var i = 0;
            while (( val /= 2 ) >= 1) i++
            return "2^" + i
        },
        cover: "2ⁿ",
        generator: function* (start) {
            let f2 = 2
            while (f2 < start) f2 *= 2
            while (true) {
                yield f2
                f2 *= 2
            }
        }
    },
    factorial: {
        name: "Factorials",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => {
            let f2 = 1
            let i = 2
            while (f2 < val) f2 *= i++
            return (i - 1) + "!"
        },
        cover: "n!",
        generator: function* (start) {
            let f2 = 1
            let i = 2
            while (f2 < start) f2 *= i++
            while (true) {
                yield f2
                f2 *= i++
            }
        }
    },
    squares: {
        name: "Square numbers",
        types: ["years", "months", "days", "hours"],
        explain: val => {
            return Math.sqrt(val) + "²"
        },
        cover: "n²",
        generator: function* (start) {
            let n = 1, i = 1
            while (n < start) {n = i*i; i++}
            while (true) {
                yield n
                n = i*i
                i++
            }
        }
    },
    cubes: {
        name: "Cube numbers",
        types: ["years", "months", "days", "hours"],
        explain: val => {
            let n = 1, i = 1
            while (n < val) {n = i*i*i; i++}
            return (i - 1) + "³"
        },
        cover: "n³",
        generator: function* (start) {
            let n = 1, i = 1
            while (n < start) {n = i*i*i; i++}
            while (true) {
                yield n
                n = i*i*i
                i++
            }
        }
    },
    hundredOfMonths: {
        name: "Hundred of months",
        types: ["years", "months"],
        cover: "10²",
        explain: val => (val / 100) + " hundred", 
        generator: function* (start) {
            let i = Math.floor(start / 100) * 100
            if (i < start) i += 100
            while (true) {
                yield i
                i += 100
            }
        }
    },
    thousandOfDays: {
        name: "Thousands of days",
        types: ["days"],
        cover: "10³",
        explain: val => (val / 1000) + " thousands", 
        generator: function* (start) {
            let i = Math.floor(start / 1000) * 1000
            if (i < start) i += 1000
            while (true) {
                yield i
                i += 1000
            }
        }
    },
    kBOfDays: {
        name: "Kilobytes of days (1024)",
        types: ["days"],
        cover: "kB",
        explain: val => (val / 1024) + " x 1024", 
        generator: function* (start) {
            let i = Math.floor(start / 1024) * 1024
            if (i < start) i += 1024
            while (true) {
                yield i
                i += 1024
            }
        }
    },
    tenThousandsOfHours: {
        name: "Ten thousands of hours",
        types: ["hours"],
        cover: "10⁴",
        generator: function* (start) {
            let i = Math.floor(start / 10000) * 10000
            if (i < start) i += 10000
            while (true) {
                yield i
                i += 10000
            }
        }
    },
    tenMillionsOfSeconds: {
        name: "Ten millions of seconds",
        types: ["seconds"],
        cover: "10⁷",
        generator: function* (start) {
            let i = Math.floor(start / 10000000) * 10000000
            if (i < start) i += 10000000
            while (true) {
                yield i
                i += 10000000
            }
        }
    },
    pi: {
        name: "Pi decimals",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => "First decimals of PI",
        cover: "π",
        generator: start => firstDigitsGenerator(pidigits, start)
    },
    e: {
        name: "e decimals (Euler's number)",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => "First decimals of e (Euler's number)",
        cover: "e",
        generator: start => firstDigitsGenerator(edigits, start)
    },
    goldNumber: {
        name: "Gold number decimals",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => "First decimals of the gold number",
        cover: "φ",
        generator: start => firstDigitsGenerator(goldnumber, start)
    },
    datesInHistory: {
        name: "Dates in history",
        types: ["days"],
        explain: val => val.name + " " + val.action + ", " + (val.gender.toLowerCase() === "m" ? "he" : "she") + " was " + val.agedays + " days.",
        getValue: obj => obj.agedays,
        cover: "fa-user",
        generator: function* (start) {
            let i = 0;
            while (history.length > i && history[i].agedays < start) i++
            if (i !== history.length) {
                while (history.length > i) {
                    yield history[i]
                    i++
                }
            }
        }
    },
    hexWords: {
        name: "Hex words",
        types: ["years", "months", "days", "hours", "minutes", "seconds"],
        explain: val => val.read + " in hexadecimal, readable number known as a Hex Word",
        getValue: obj => obj.value,
        cover: "0x",
        generator: function* (start) {
            let i = 0;
            while (hexwords.length > i && hexwords[i].value < start) i++
            if (i !== hexwords.length) {
                while (hexwords.length > i) {
                    yield hexwords[i]
                    i++
                }
            }
        }
    }
    /*
    666, special numbers, mersenne, parfaits, palyndromes, dates remarquables (untel a fait ci à tel age / date), nombre repetitifs 11111 22222
    */
}

const pidigits   = "314159265358979323846"
const edigits    = "271828182845904523536"
const goldnumber = "161803398874989484820"

/**
 * Generator yielding numbers after start matching the begining of digits from digs
 * @param {*} digs 
 * @param {*} start 
 */
const firstDigitsGenerator = function* (digs, start) {
    let d = digits(start)
    while (true) {
        let num = firstdigits(digs, d)
        if (num >= start) yield num
        d++
    }
}
const firstdigits = (digs, nb) => {
    return Number(digs.substr(0, nb)) 
}
const digits = n => {
    var i = 1;
    while (( n /= 10 ) >= 1) i++
    return i;
}
const isPrime = num => {
    for (let i = 2, s = Math.sqrt(num); i <= s; i++)
        if (num % i === 0) return false;
    return num !== 1 && num !== 0;
}

const history = [
    {
        name: "Bill Gates",
        gender: "m",
        action: "registered the name Micro-soft",
        on: "Nov 26, 1976",
        birth: "Oct 28, 1955"
    },
    {
        name: "Ferdinand Magellan",
        gender: "m",
        action: "began crossing the Pacific Ocean",
        on: "Nov 28, 1520",
        birth: "Feb 3, 1480"
    },
    {
        name: "Garry Kasparov",
        gender: "m",
        action: "participated in the first chess match between man and machine",
        on: "Jan 5, 2003",
        birth: "Apr 13, 1963"
    },
    {
        name: "Jimmy Wales",
        gender: "m",
        action: "launched Wikipedia with Larry Sanger",
        on: "Jan 15, 2001",
        birth: "Aug 7, 1966"
    },
    {
        name: "Steve Jobs",
        gender: "m",
        action: "launched the first Macintosh",
        on: "Jan 24, 1984",
        birth: "Feb 24, 1955"
    },
    {
        name: "Neil Armstrong",
        gender: "m",
        action: "step on the moon",
        on: "Jul 21, 1969",
        birth: "Aug 5, 1930"
    }
].map(a => {
    return {...a, agedays: moment(a.on).diff(a.birth, "days")}
}).sort((a, b) => {
    return a.agedays - b.agedays
})

const hexwords = [
    "acce55ed",
    "acc01ade",
    "ace",
    "ac7ed",
    "ad0be",
    "affec7ed",
    "ba5eba11",
    "ba11ade",
    "ba0bab",
    "bedabb1e",
    "be11",
    "be5077ed",
    "b0a710ad",
    "b01dface",
    "b1ade",
    "b1eed",
    "b1e55ed",
    "b100d",
    "b01dface",
    "b00b",
    "b007ab1e",
    "cab005e",
    "ca11ab1e",
    "cab1e",
    "caca0",
    "cafe",
    "ca55e77e",
    "c01d",
    "c01de57",
    "c0ffee",
    "c0ded",
    "ca5cade",
    "deadbea7",
    "dec0ded",
    "decade",
    "dec0ded",
    "dec0de",
    "defec8",
    "deaf",
    "dead",
    "f01dab1e",
    "f005ba11",
    "facade",
    "fab1e",
    "face",
    "faced",
    "fade",
    "faded",
    "0ddba11",
    "5ca1ab1e",
    "7e1eca57",
    "1abe1ed",
    "0b57ac1e"
].map(h => {
    return {read: h, value: parseInt(h, 16)}
}).sort((a, b) => a.value - b.value)

const specials = [
    {
        name: "Beast number",
        link: "https://en.wikipedia.org/wiki/Number_of_the_Beast",
        values: [666]
    },
    {
        name: "Perfect number",
        link: "https://en.wikipedia.org/wiki/Perfect_number",
        values: [6, 28, 496, 8128, 33550336, 8589869056, 137438691328] // there is more which can't be involved in time counting
    },
    {
        name: "Taxicab number",
        link: "https://en.wikipedia.org/wiki/Taxicab_number",
        values: [2, 1729, 87539319, 6963472309248] // there is more which can't be involved in time counting
    },
    
]